#Corruptor
#dillon9

import binascii

def corrupt(filename):

	x = open(filename,"rb")
	t = x.read()
	x.close()
	t = "".join(t.split())
	stuff = binascii.hexlify(t)

	stuff = list(stuff)
	stuff[37] = str(6)
	stuff = "".join(stuff)
	stuff = ' '.join([stuff[i:i+4] for i in range(0, len(stuff), 4)])

	x = open(filename,"w")
	x.write(stuff)
	x.close()

corrupt(raw_input("File to corrupt: "))